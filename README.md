# Easy Strings

[![Netlify Status](https://api.netlify.com/api/v1/badges/c427f194-74aa-424c-b986-97c32f3bddee/deploy-status)](https://app.netlify.com/sites/easy-string-api/deploys)

A collection of commonly used functions for strings for Fortran.

*Suggestions and code contributions are welcome.*

```fortran
use easy_string_m
```

## Build with [Fortran-lang/fpm](https://github.com/fortran-lang/fpm)

```
fpm run --example --all
```

```toml
[dependencies]
easy-string = { git = "https://gitee.com/fortran-stack/easy-string" }
```

## API Documentation

```sh
ford FORD-project-file.md
```

Latest API documentation is available at [easy-string-api](https://easy-string-api.netlify.app/). 

## Reference

- [fortran-lang/stdlib](https://github.com/fortran-lang/stdlib)
- [fortran-lang/fpm](https://github.com/fortran-lang/fpm)
- [fortran-fans/forlab](https://github.com/fortran-fans/forlab)
- [everythingfunctional/strff](https://gitlab.com/everythingfunctional/strff/)
