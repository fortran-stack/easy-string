program main

    use string_disp_m, only: disp
    
    integer :: i(5) = [110, 2, 45678910, 10, 45]
    call disp(i, "i = ", "i10")
    call disp(spread(i+10, dim=1, ncopies=3), "spread(i, dim=1, ncopies=3) = ", "i0")
    call disp(real(spread(i-11, dim=1, ncopies=3)), "real(spread(i, dim=1, ncopies=3)) = ", "g11.4")
    call disp(.true., ".true. = ")

end program main

