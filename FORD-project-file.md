---
project: fortran-stack/easy-string
summary: 字符串操作
project_website: https://gitee.com/fortran-stack/easy-string
project_download: https://gitee.com/fortran-stack/easy-string/releases
output_dir: API-html
media_dir: doc/media
author: 左志华
author_description: 哈尔滨工程大学-船舶与海洋结构物设计制造，在读学生
email: zuo.zhihua@qq.com
author_pic: ./media/zoziha.png
website: https://gitee.com/zoziha
preprocess: false
display: public
source: true
graph: false
parallel: 4
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
---

{!README.md!}

{!README-CN.md!}
