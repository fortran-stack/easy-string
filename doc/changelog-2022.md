title: 日志 2022
---

## 日志

- 使用 `class(*)` 构建 `to_string` 函数，而不是接口重载；
- 重命名库：`easy_string` -> `easy-string`；
- 添加 `file_name` 函数查询文件名；
- 添加 `disp` 子例程显示数组内容；

## 代办
